# Đường dẫn đến thư mục chứa video từ folder A và folder B
$folderA = ".\folder_a"
$folderB = ".\folder_b"

# Lấy danh sách tất cả các tệp video trong folder A và folder B
$videosA = Get-ChildItem -Path $folderA -Filter *.mp4
$videosB = Get-ChildItem -Path $folderB -Filter *.mp4

# Đường dẫn đến thư mục đầu ra
$outputFolder = ".\folder_output"
New-Item -Path $outputFolder -ItemType Directory -Force

# Lặp qua từng video từ folder A
foreach ($videoA in $videosA) {
    $videoAName = $videoA.Name
    
    # Lặp qua từng video từ folder B
    foreach ($videoB in $videosB) {
        $videoBName = $videoB.Name
        
        # Tạo tên tệp đầu ra
        $outputVideoName = "${videoAName}_with_${videoBName}"

        # Sử dụng FFmpeg để ghép video từ A với video từ B
		./ffmpeg -i "$folderA\$videoAName" -i "$folderB\$videoBName" -filter_complex "[0:v][0:a][1:v][1:a]concat=n=2:v=1:a=1[outv][outa]" -map "[outv]" -map "[outa]" "$outputFolder\$outputVideoName"
    }
}

Write-Host "Done..........!"