$folderA = ".\folder_a"
$folderB = ".\folder_b"

# get list
$videosA = Get-ChildItem -Path $folderA -Filter *.mp4
$videosB = Get-ChildItem -Path $folderB -Filter *.mp4

# output folder
$outputFolder = ".\folder_output"
New-Item -Path $outputFolder -ItemType Directory -Force

for ($i = 0; $i -lt $videosA.Count; $i++) {
    $videoA = $videosA[$i]
    $videoB = $videosB[$i]

    $videoAName = $videoA.Name
    $videoBName = $videoB.Name

    $outputVideoName = "video-${i}__${videoAName}"

    ./ffmpeg -i "$folderA\$videoAName" -i "$folderB\$videoBName" -filter_complex "[0:v][0:a][1:v][1:a]concat=n=2:v=1:a=1[outv][outa]" -map "[outv]" -map "[outa]" "$outputFolder\$outputVideoName"
}

