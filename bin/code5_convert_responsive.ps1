﻿$folderConvertPath = ".\folder_convert_responsive"

$folderConvertItems = Get-ChildItem -Path $folderConvertPath


for ($i = 0; $i -lt $folderConvertItems.Count; $i++) {
    $item = $folderConvertItems[$i]
    $itemName = $item.Name
    $outputName = "convert-${i}__${itemName}"
    .\ffmpeg -i "$folderConvertPath\$itemName" -s 1280x720 -c:a copy "$folderConvertPath\$outputName"
}

Write-Host "Done........"
