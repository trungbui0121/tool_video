# Đường dẫn đến thư mục chứa các tệp bạn muốn đổi tên
$folderPath = ".\folder_a"

# Lấy danh sách tất cả các tệp trong thư mục
$files = Get-ChildItem -Path $folderPath

# Lặp qua từng tệp và đổi tên
for ($i = 0; $i -lt $files.Count; $i++) {
    $newName = "new2_${i}.txt"
    $file = $files[$i]
    $file = $files[$i]

    # Đường dẫn đầy đủ đến tệp cũ và tệp mới
    $oldFilePath = $file.FullName
#    $newFilePath = Join-Path -Path $folderPath -ChildPath $newName
    # Sử dụng Rename-Item để đổi tên tệp
    Rename-Item -Path $oldFilePath -NewName $newName
}

Write-Host "Done!"
