$folderA = "./folder_a"
$folderB = "./folder_b"


$videosA = Get-ChildItem -Path $folderA -Filter *.mp4
$videosB = Get-ChildItem -Path $folderB -Filter *.mp4


$outputFolder = "./folder_output"
New-Item -Path $outputFolder -ItemType Directory -Force


for ($i = 0; $i -lt $videosA.Count; $i++) {
    $videoA = $videosA[$i]
    $videoB = $videosB[$i]

    $videoAName = $videoA.Name
    $videoBName = $videoB.Name

    $outputVideoName = "video-${i}__${videoAName}"


    ./ffmpeg -i "$folderA\$videoAName" -i "$folderB\$videoBName" -filter_complex "[0:v]scale=1280:720[v0];[1:v]scale=1280:720[v1];[v0][0:a][v1][1:a]concat=n=2:v=1:a=1[outv][outa]" -map "[outv]" -map "[outa]" "$outputFolder\$outputVideoName"
}

Write-Host "Done........................."
