$folderA = "./folder_a"
$folderB = "./folder_b"

$videosA = Get-ChildItem -Path $folderA -Filter *.mp4
$videosB = Get-ChildItem -Path $folderB -Filter *.mp4

$outputFolder = "./folder_output"
New-Item -Path $outputFolder -ItemType Directory -Force

for ($i = 0; $i -lt $videosA.Count; $i++) {
    $videoA = $videosA[$i]
    $videoB = $videosB[$i]

    $videoAName = $videoA.Name
    $videoBName = $videoB.Name

    $outputConcatName = "concat-${i}__${videoAName}"
    $outputConcatNameChange = "change_${i}___${videoAName}"

    $videoSpeed = 1.5
    $videoSpeedReverse = 1 / $videoSpeed
    $audioTone = 1.5
    $audioToneReverse = 1 / $audioTone

#concat
    ./ffmpeg -y -i "$folderA\$videoAName" -i "$folderB\$videoBName" -filter_complex "[0:v]scale=1280:720[v0];[1:v]scale=1280:720[v1];[v0][0:a][v1][1:a]concat=n=2:v=1:a=1[outv][outa]" -map "[outv]" -map "[outa]" -ar 44100 "$outputFolder\$outputConcatName"
#change audio
    ./ffmpeg -y -i $outputFolder\$outputConcatName -af "atempo=${audioToneReverse}, asetrate=44100*${audioTone}" $outputFolder\$outputConcatNameChange
#add overlay
    ./ffmpeg -y -i "$outputFolder\$outputConcatNameChange" -stream_loop -1 -i .\logo\overlay.gif -filter_complex "[0:v]setsar=sar=1[v]; [v][1]blend=all_mode='overlay':all_opacity=0.7 :shortest=1" -movflags +faststart -aspect 9:16 "$outputFolder\$outputConcatName"
#change speed
    ./ffmpeg -y -i "$outputFolder\$outputConcatName" -vf "setpts=${videoSpeedReverse}*PTS" -af "atempo=${videoSpeed}" "$outputFolder\$outputConcatNameChange"

    rm $outputFolder\$outputConcatName
}

Write-Host "------------------------->>> DONE <<<--------------------------"
