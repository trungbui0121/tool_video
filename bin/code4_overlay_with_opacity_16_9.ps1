﻿$folderA = ".\folder_a"
$folderB = ".\folder_b"

$videosA = Get-ChildItem -Path $folderA -Filter *.mp4
$videosB = Get-ChildItem -Path $folderB -Filter *.mp4

$outputFolder = ".\folder_output"
New-Item -Path $outputFolder -ItemType Directory -Force

# Chuyển đổi và ghép video với lớp phủ và độ trong suốt
for ($i = 0; $i -lt $videosA.Count; $i++) {
    $videoA = $videosA[$i]
    $videoB = $videosB[$i]

    $videoAName = $videoA.Name
    $videoBName = $videoB.Name

    $outputVideoName = "video-${i}__${videoAName}"

    # tạo lớp phủ với opacity
    .\ffmpeg -n -i "$folderA\$videoAName" -stream_loop -1 -i .\logo\overlay.gif -filter_complex "[0:v]setsar=sar=1[v]; [v][1]blend=all_mode='overlay':all_opacity=0.7 :shortest=1" -movflags +faststart -aspect 16:9 "$outputFolder\$outputVideoName"
}

Write-Host "Done........"
